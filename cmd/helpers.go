package cmd

import (
	"math"
)

func getMonthlyPayment(initialPrincipal float64, interestRate float64, loanTerm int) float64 {
	// Convert loan term to float64
	term := float64(loanTerm)

	return initialPrincipal * math.Pow(1+interestRate, term) * interestRate / (math.Pow(1+interestRate, term) - 1)
}

func calculateRemainingPrincipal(initialPrincipal float64, interestRate float64, monthlyPayment float64, month int) float64 {
	monthFloat := float64(month)

	return initialPrincipal*math.Pow(1+interestRate, monthFloat) - (monthlyPayment*(math.Pow(1+interestRate, monthFloat)-1))/interestRate
}
