/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// calculateCmd represents the calculate command
var calculateCmd = &cobra.Command{
	Use:   "monthly-payment",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		// Get args
		rate, _ := cmd.Flags().GetFloat64("interest-rate")
		price, _ := cmd.Flags().GetFloat64("price")
		term, _ := cmd.Flags().GetInt("loan-term")
		downPayment, _ := cmd.Flags().GetFloat64("down-payment")
		rate /= 12
		result := getMonthlyPayment(price-downPayment, rate, term*12)
		fmt.Printf("$%0.2f\n", result)
	},
}

func init() {
	rootCmd.AddCommand(calculateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// calculateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	calculateCmd.Flags().Float64P("interest-rate", "i", 0, "Help message for interest-rate")
	calculateCmd.MarkFlagRequired("interest-rate")
	calculateCmd.Flags().Float64P("price", "p", 0, "Help message for price")
	calculateCmd.MarkFlagRequired("price")
	calculateCmd.Flags().IntP("loan-term", "t", 0, "Help message for loan-term")
	calculateCmd.MarkFlagRequired("loan-term")
	calculateCmd.Flags().Float64P("down-payment", "d", 0, "Help message for down-payment")
}
