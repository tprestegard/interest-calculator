/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// remainingBalanceCmd represents the remainingBalance command
var remainingBalanceCmd = &cobra.Command{
	Use:   "remaining-balance",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Get args
		rate, _ := cmd.Flags().GetFloat64("interest-rate")
		price, _ := cmd.Flags().GetFloat64("price")
		term, _ := cmd.Flags().GetInt("loan-term")
		month, _ := cmd.Flags().GetInt("month")
		rate /= 12
		monthlyPayment := getMonthlyPayment(price, rate, term*12)
		result := calculateRemainingPrincipal(price, rate, monthlyPayment, month)
		fmt.Printf("Remaining balance after month %d: $%0.2f\n", month, result)
	},
}

func init() {
	rootCmd.AddCommand(remainingBalanceCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// remainingBalanceCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// remainingBalanceCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	remainingBalanceCmd.Flags().Float64P("interest-rate", "i", 0, "Help message for interest-rate")
	remainingBalanceCmd.MarkFlagRequired("interest-rate")
	remainingBalanceCmd.Flags().Float64P("price", "p", 0, "Help message for price")
	remainingBalanceCmd.MarkFlagRequired("price")
	remainingBalanceCmd.Flags().IntP("loan-term", "t", 0, "Help message for loan-term")
	remainingBalanceCmd.MarkFlagRequired("loan-term")
	remainingBalanceCmd.Flags().IntP("month", "m", 0, "Help message for month")
	remainingBalanceCmd.MarkFlagRequired("month")
}
